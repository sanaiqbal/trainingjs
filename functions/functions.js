//Functions
//Defining and calling functions
function fun()
{
    console.log('1st function is called');
}
fun();
function abc(var1)
{
    console.log('2nd function is called with val '+var1);
}
var ver =10;
abc(ver);

function abc2(var2)
{
    var2=var2+5;
    return var2;
}
var ret=abc2(10);
console.log('3rd function return '+ ret);

//function scope
//global scope
const num1 = 20;
const num2 = 3;
const name = 'kh';

// This function is defined in the global scope
function multiply() {
  return num1 * num2;
}

console.log('Global scope '+multiply()); // Returns 60

// A nested function example
function getScore() {
  const num1 = 2;
  const num2 = 3;

  function add() {
    return name + ' scored ' + (num1 + num2);
  }

  return add();
}

console.log('Nested function '+getScore()); 
//Recursive functions
function loop(val)
{
    if(val==5)
    {
        console.log('Recursive function return value '+val);
        return;
    }
    loop(val+1);
    
}
loop(0);


//Converting recursive function to non-recursive.
function foo(i) {
    if (i < 0) {
      return;
    }
    console.log('begin: ' + i);
    foo(i - 1);
    console.log('end: ' + i);
  }
  foo(3);

  //Nested functions
  function outside(x) {
    function inside(y) {
      return x + y;
    }
    return inside;
  }
  const fnInside = outside(3);
  const result = fnInside(5);
  const result1 = outside(3)(5);
  console.log('Nested Functions');
  console.log('Result of 1st input: '+ result);
  console.log('Result of 2nd input: '+ result1);

  //Multiple nested loops 
  function A(x) {
    function B(y) {
      function C(z) {
        console.log('Nested functions output: '+(x + y + z));
      }
      C(3);//This gives value to z
    }
    B(2);//This gives value to y 
  }
  A(1);//This will gives value to x and
  // this calls all the functions and return value 6



  //Closure
  //if the return type is function 
  const pet = function (name) {  
    const getName = function () {
      return name;
    }
    return getName; // Return the inner function, thereby exposing it to outer scopes
  }
  const myPet = pet('Vivie');
  
  console.log('Closure result: '+myPet()); 

//Using parameters
function myConcat(separator) {
  let result = ''; // initialize list
  // iterate through arguments
  for (let i = 1; i < arguments.length; i++) {
    result += arguments[i] + separator;
  }
  return result;
}
console.log('argument1 output: '+myConcat(', ', 'red', 'orange', 'blue'));
console.log('argument2 output: '+myConcat('; ', 'elephant', 'giraffe', 'lion', 'cheetah'));

//Function parameters
//Without default parameters
function multiply(a, b) {
  b = typeof b !== 'undefined' ?  5 : b;
  return a * b;
}
console.log('without default parametrs '+multiply(5));

//With default parameters
function multiply1(a1, b1 = 1) {
  return a1 * b1;
}
console.log('with default parameters '+multiply1(5));
//rest parameters
function multiply(multiplier, ...theArgs) {//...theArgs is the rest parameters
  return theArgs.map((x) => multiplier * x);
}
//it uses 1st as multiplier as defined in function and
//then the rest will be used as rest parameters
const arr = multiply(2, 1, 2, 3);
console.log('by using rest parameters: '+arr); 


//arrow functions
const a = [
  'Hydrogen',
  'Helium',
  'Lithium',
  'Beryllium'
];
console.log('arrow functions:');
const a2 = a.map(function(s) { return s.length; });
console.log(a2); // logs [8, 6, 7, 9]
const a3 = a.map((s) => s.length);
console.log(a3);
console.log('');

//predefined functions
console.log('predefined functions:');
console.log('represented as string '+eval(new String('2+2')));
console.log('check number finite is: '+isFinite(10));
console.log('check the value is Nan: '+Number.isNaN(1/0));
console.log('convert string to float'+parseFloat("20.01"));
console.log('return integer of specific radix: '+parseInt("5 4 6 4"));
let uri = "my test.asp?name=ståle&car=saab";
let encoded = encodeURI(uri);
console.log('encoded uri: '+encoded);
let decoded = decodeURI(uri);
console.log('decoded uri: '+decoded);
let encodecomp = encodeURIComponent(uri);
console.log('encoded uri component: '+encodecomp);
let decodecomp = decodeURIComponent(uri);
console.log('decoded uri component: '+decodecomp);
let esc = escape(uri);
console.log('escape val: '+esc);
let unesc = unescape(uri);
console.log('unescape val:'+ unesc);


