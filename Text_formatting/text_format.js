//strings
var chr='abc';
var chr2="abc";
console.log(chr+' '+chr2);
const paragraph = 'The quick brown fox jumps over the lazy dog. If the dog barked, was it really lazy?';
const searchTerm = 'dog';
const indexOfFirst = paragraph.indexOf(searchTerm);
console.log(`The index of the first "${searchTerm}" from the beginning is ${indexOfFirst}`);
console.log(`The index of the 2nd "${searchTerm}" is ${paragraph.indexOf(searchTerm, (indexOfFirst + 1))}`);

//concatination 
const str1 = 'Hello';
const str2 = 'World';
console.log(str1.concat(' ', str2));
console.log(str2.concat(', ', str1));

//split
const str = 'The quick brown fox jumps over the lazy dog.';
const words = str.split(' ');
console.log(words[3]);
const chars = str.split('');
console.log(chars[8]);
const strCopy = str.split();
console.log(strCopy);

//tolowercase
const sentence = 'The quick brown fox jumps over the lazy dog.';
console.log(sentence.toLowerCase());

//multiline
console.log('string text line 1\n\
string text line 2');
