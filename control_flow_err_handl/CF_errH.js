//Control Flow and Error handling
//Block Statement
//Block statements are commonly used
// with control flow statements (if, for, while).
//Condition Statements
//if ....else
const b = new Boolean(false);
if (b)
{
    console.log("b is "+b);
}        // this condition evaluates to true
if (b == true)
{
    console.log("b is "+b);
} // this condition evaluates to false

//Switch Statement
var fruitType='Oranges'
switch (fruitType) {
    case 'Oranges':
      console.log('Oranges are $0.59 a pound.');
      break;
    case 'Apples':
      console.log('Apples are $0.32 a pound.');
      break;
    case 'Bananas':
      console.log('Bananas are $0.48 a pound.');
      break;
    case 'Cherries':
      console.log('Cherries are $3.00 a pound.');
      break;
    case 'Mangoes':
      console.log('Mangoes are $0.56 a pound.');
      break;
    case 'Papayas':
      console.log('Mangoes and papayas are $2.79 a pound.');
      break;
    default:
      console.log(`Sorry, we are out of ${fruitType}.`);
  }
 

//Exception handling
//try.....catch
var monthName='kk';
function getMonthName(mo) {
  mo = mo - 1; // Adjust month number for array index (1 = Jan, 12 = Dec)
  let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul',
                'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  if (months[mo]) {
    return months[mo];
  } else {
    throw 'InvalidMonthNo'; // throw keyword is used here
  }
}

try { // statements to try
  monthName = getMonthName(myMonth); // function could throw exception
}
catch (e) {
  monthName = 'unknown';
  logMyErrors(e); // pass exception object to error handler (i.e. your own function)
}

function logMyErrors(e) 
{
    console.log('Exception occur and control goes to catch');
}

//By try...catch...finaly
function f() {
    try {
      console.log(0);
      throw 'bogus';
    } catch(e) {
      console.log(1);
      return true;    // this return statement is suspended
                      // until finally block has completed
      console.log(2); // not reachable
    } finally {
      console.log(3);
      return false;   // overwrites the previous "return"
      console.log(4); // not reachable
    }
    // "return false" is executed now
    console.log(5);   // not reachable
  }
  console.log(f());