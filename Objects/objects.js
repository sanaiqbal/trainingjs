//Objects
const myCar = new Object();
myCar['make'] = 'Ford';
myCar.model = 'Mustang';
myCar.year = 1969;
myCar.color = 'black';
console.log('Object properties: '+myCar.make+' '+myCar.color+' '+myCar.model);

//properties
let propertyName = 'make';
myCar[propertyName] = 'Ford';

// access different properties by changing the contents of the variable
propertyName = 'model';
myCar[propertyName] = 'Mustang';
console.log(myCar);

//object properties traversion
const obj = {a: 1, b: 2, c: 3};

for (const prop in obj) {
  console.log(`${prop} = ${obj[prop]}`);
}

//object creation using function
function Car(make, model, year, owner) {
    this.make = make;
    this.model = model;
    this.year = year;
    this.owner = owner;
  }
const car1 = new Car('Eagle', 'Talon TSi', 1993, 'rand');
const car2 = new Car('Nissan', '300ZX', 1992, 'ken');
console.log('car1: '+car1.make,' ',car1.model,' ',car1.owner,' ',car1.year);
console.log('car2: '+car2.make,' ',car2.model,' ',car2.owner,' ',car2.year);


//object creation using Object.create()
const Animal = {
    type: 'Invertebrates', // Default value of properties
    displayType() {  // Method which will display type of Animal
      console.log('Type of animal:',this.type);
    }
  };
  
  // Create new animal type called animal1
  const animal1 = Object.create(Animal);
  animal1.displayType(); // Output: Invertebrates
  
  // Create new animal type called fish
  const fish = Object.create(Animal);
  fish.type = 'Fishes';
  fish.displayType();

  //getter and setter
  const myObj = {
    a: 7,
    get b() {
      return this.a + 1;
    },
    set c(x) {
      this.a = x / 2;
    }
  };
  
  console.log('a: ',myObj.a); // 7
  console.log('b: ',myObj.b); // 8 <-- At this point the get b() method is initiated.
  myObj.c = 50;         //   <-- At this point the set c(x) method is initiated
  console.log('c: ',myObj.b); 

  //deleting property
const myobj = new Object();
myobj.a = 5;
myobj.b = 12;
// Removes the a property, leaving myobj with only the b property.
delete myobj.a;
console.log ('a exist in object: ','a' in myobj); 

//comparing objects
const fruit = {name: 'apple'};
const fruitbear = {name: 'apple'};
console.log('check if these two objects are same: ',fruit == fruitbear); 
console.log('check if these two objects are same: ',fruit === fruitbear); 