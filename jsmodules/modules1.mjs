//anynomus function
export default function()
{
   console.log('Default function must called whenever the module imported');
}
function add(a,b)
{
    return a+b;
}
function multiply(a,b)
{
   return a*b;
}
//calss 
class square
{
    
    constructor(a)
    { 
        this.a = a*a;
       console.log('Constructor square is: '+a*a);
    }
    method()
    {
        console.log('class method');
    }
    

}

export {square};
export { add,multiply};
//export { multiply};