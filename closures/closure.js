//closures
function makeFunc() {
    const name = 'Name';
    function displayName() {
      console.log(name);//this uses the outer function's variable 'name'
    }
    return displayName;
  } 
const myFunc = makeFunc();
myFunc();



function makeAdder(x) {
    return function (y) {
      return x + y;
    };
  }
  
  const add5 = makeAdder(5);//it first take 5 for x and then when it calls with 2
  // it assign 2 to y then return 7
  const add10 = makeAdder(10);//it take 10 for x and then when calls with 2 
  //it assign 2 to y then return 12
  console.log('addition:');
  console.log(add5(2)); // 7
  console.log(add10(2)); // 12



//private methods with closure
const counter = (function () {
    let privateCounter = 0;
    function changeBy(val) {
      privateCounter += val;
    }
  
    return {
      increment() {
        changeBy(1);
      },
  
      decrement() {
        changeBy(-1);
      },
  
      value() {
        return privateCounter;
      },
    };
  })();
  console.log('private methods:')
  console.log(counter.value()); // 0.
  
  counter.increment();
  counter.increment();
  console.log(counter.value()); // 2.
  
  counter.decrement();
  console.log(counter.value()); // 1.
  

//scope chain of closure
// global scope
const e = 10;
function sum(a) {
  return function (b) {
    return function (c) {
      // outer functions scope
      return function (d) {
        // local scope
        return a + b + c + d + e;
      };
    };
  };
}
console.log('scope chain of closure');
console.log(sum(1)(2)(3)(4)); // log 20


