//Numbers
//Decimal
var val = 627181;
//binary 
const v=1010101010011101;
//octal
const n = 0755;
const a = 0o10; 
//hexadecimal
//var b=0x1RU7661;

//Number Object
const biggestNum = Number.MAX_VALUE;
const smallestNum = Number.MIN_VALUE;
const infiniteNum = Number.POSITIVE_INFINITY;
const negInfiniteNum = Number.NEGATIVE_INFINITY;
const notANum = Number.NaN;
//Math Object
console.log(Math.PI);
console.log(Math.max(5,4));
console.log(Math.pow(2,3)); 
//BigInt
const b1 = BigInt(123);//converting number to bigint
var v2 = 2n+3n;
console.log(b1);
console.log(v2);
//Date object
const Xmas95 = new Date('December 25, 1995');
console.log(Xmas95);
