- Numbers
- Decimal numbers
- var b=872389;
- decimal literals can start with a zero (0) followed by another decimal digit
- binary numbers ->consists of 1's and 0's
- octal numbers
- Octal number syntax uses a leading zero.from 0 to 7
- octal numbers are supported if they are prefixed with 0o  const a = 0o10; 
- hexadecimal
- var b=0x1RU7661; starts with 0x
- Exponential
- 2e6    consists of e E
- Number Object
- The built-in Number object has properties for numerical constants, such as maximum value, not-a-number, and infinity.
- const biggestNum = Number.MAX_VALUE;
- const smallestNum = Number.MIN_VALUE;
- const infiniteNum = Number.POSITIVE_INFINITY;
- const negInfiniteNum = Number.NEGATIVE_INFINITY;
- const notANum = Number.NaN;
- Math Object
- The built-in Math object has properties and methods for mathematical constants and functions. For example, the Math object's PI property has the value of pi (3.141…), 
- //BigInt
- const b1 = BigInt(123);//converting number to bigint
- var v2 = 2n+3n;
- //Date object
- const Xmas95 = new Date('December 25, 1995');
