//Expressions and operators
//Assignment operators
// there are lots of assign operator types 
//like addition assign subtract assign but here are some examples
console.log('assignment operators');
let n=5;
function f()
{
    return 20;
}
n+=f();
console.log('val1 :'+n);
n-=f();
console.log('val2 :'+n);
n*=f();
console.log('val3 :'+n);
n/=f();
console.log('val4 :'+n);
n%=f();
console.log('val5 :'+n);
//Comparison operators
console.log('comaprison operators');
var val1=19;
var val2=12;
var var1=35;
var var2=35;
if(val1>val2)
{
    console.log('val1 > val2');
}
if(val1!=val2)
{
    console.log('val1 ! = val2');
}
if(var1==var2)
{
    console.log('var1 = var2');
}
if(var1<=var2)
{
    console.log('var1 <= var2');
}
//arithmetic operators
console.log('arithmetic operators');
val2++;
val1--;
var1**2;
var2**2;
console.log('val1 is: '+val1);
console.log('val2 is: '+val2);
console.log('var1 is: '+var1);
console.log('var2 is: '+var2);
//bitwise operators
console.log('bitwise operators');
console.log(var1&var2);
console.log(val1|val2);
console.log(var1^var2);
console.log(~var2);
//shift bitwise operators
console.log(var1<<var2);
//logical operators
console.log('logical operators');
console.log(var1&&var2);
console.log(var1||vaar2);
console.log(!var2);
//bigint
console.log('bigint');
let v=2n+3n;
v=2n+1n;
console.log('bigint value: '+v);
//string operator
console.log('string operator');
let mystring = 'alpha';
mystring += 'bet';
console.log('val: '+mystring);
//conditional ternary operators
console.log('conditional/ternary operators');
var age=21;
const status = age >= 18 ? 'adult' : 'minor';
console.log('val: '+status);
//delete operator
const myObj = {h: 4};
delete myObj.h;
//typeof
//it returns the type of variable
const myFun = new Function('5 + 2');
const shape = 'round';
console.log('type: '+typeof myFun);
console.log('type: '+typeof shape);
//relational operators
//if the given value  is the property of the array then it returns true.
const trees = ['redwood', 'bay', 'cedar', 'oak', 'maple'];
0 in trees;        // returns true
3 in trees;        // returns true
'bay' in trees;    // returns false (you must specify the index number,
//basic operators
//this
this.propertyName;
//()
2+(3*2);



