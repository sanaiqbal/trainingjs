//iterators
function makeRangeIterator(start = 0, end = Infinity, step = 1) {
    let nextIndex = start;
    let iterationCount = 0;
  
    const rangeIterator = {
      next() {
        let result;
        if (nextIndex < end) {
          result = { value: nextIndex, done: false };
          nextIndex += step;
          iterationCount++;
          return result;
        }
        return { value: iterationCount, done: true };
      }
    };
    return rangeIterator;
  }
  
const it = makeRangeIterator(1, 10, 2);
let result = it.next();
while (!result.done) {
 console.log(result.value); // 1 3 5 7 9
 result = it.next();
}
console.log("Iterated over sequence of size: ", result.value); 


//iterables 
//it is the behavior of the iterators
for (const value of ['a', 'b', 'c']) {
    console.log(value);
  }
  [...'abc'];
  function* gen() {
    yield* ['a', 'b', 'c'];
  }
  gen().next();
  [a, b, c] = new Set(['a', 'b', 'c']);
  a;


//you can give value to next() method, 
//which can be used to modify the internal state of the generator.
//The value passes to next() will be received by yield
function* fibonacci() {
    let current = 0;
    let next = 1;
    while (true) {
      const reset = yield current;
      [current, next] = [next, next + current];
      if (reset) {
        current = 0;
        next = 1;
      }
    }
  }
  
  const sequence = fibonacci();
  console.log(sequence.next().value);    
  console.log(sequence.next().value);     
  console.log(sequence.next().value);   
  console.log(sequence.next().value);   
  console.log(sequence.next().value);     
  console.log(sequence.next().value);     
  console.log(sequence.next().value);     
  console.log(sequence.next(true).value); 
  console.log(sequence.next().value);   
  console.log(sequence.next().value);    
  console.log(sequence.next().value);     
  

  
