- arrays
- we can use array to add multiple data 
- let arr=new Array(5);
- arr = [1,2,3,4,5];
- you can use loops to access each value of array through index
- there are many array methods that you can use 
- const list = arr2.join(' - ')
- arr=arr.concat(''+arr2);
- you can make 2D arrays 
- buffer array
- you can create buffer of array of any size and then can view the buffer array in different format like Int32Array(buffer)
- const buffer = new ArrayBuffer(12);
- const view = new Int32Array(buffer);
- console.log('buffer array: '+view);
