//arrays
let arr=new Array(5);
let arr1=Array(7);
let arr2=[];
arr2.length=9;
arr = [1,2,3,4,5];
arr1=[8,3,5,6,7,4,2];
arr2=['2','4','8'];
for (let i = 0; i < arr.length; i++) {
    console.log('arr: '+arr[i])
  }
  console.log('');
  for (let i = 0; i < arr2.length; i++) {
    console.log('arr2: '+arr2[i])
  }
  console.log('');
  arr1.forEach(function(arr1) {
    console.log('arr1: '+arr1);
  })
//array methods
arr=arr.concat(''+arr2);
console.log('arr: '+arr);
const list = arr2.join(' - ')
console.log(list);
console.log('sorted array: '+arr2.sort());
console.log('index of 5: '+arr.indexOf(5));
console.log('last index of 2: '+arr.lastIndexOf(2));
//2D array
let a = new Array(4)
for (let i = 0; i < 4; i++) {
  a[i] = new Array(4)
  for (let j = 0; j < 4; j++) {
    a[i][j] = '[' + i + ', ' + j + ']'
  }
}
for(let i=0;i<4;i++)
{
    console.log('row: '+i);
    for (let j = 0; j < 4; j++) 
    {
        console.log('col: '+a[i][j]);
    }
}
//buffer array
const buffer = new ArrayBuffer(12);
const view = new Int32Array(buffer);
console.log('buffer array: '+view);
