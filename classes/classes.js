//classes
//class and its constructor
class Color {
    constructor(r, g, b) {
      // Assign the RGB values as a property of `this`.
      this.values = [r, g, b];
    }
  }
  const red = new Color(255, 0, 0);
  console.log('values: ',red.values);
//do not return from the constructor or it will show undefine
  class MyClass {
    constructor() {
      this.myField = "foo";
      //return {};
    }
  }
  console.log('myField: ',new MyClass().myField);


//instance methods
  class Colors {
    constructor(r, g, b) {
      this.values = [r, g, b];
    }
    //getter setter methods 
    getRed() {
      return this.values[0];
    }
    setRed(value) {
      this.values[0] = value;
    }
  }
  const red1 = new Colors(255, 0, 0);//creating instance of class colors named red1
  red1.setRed(0); //calling the set method of class colors
  console.log(red1.getRed()); //calling the get method of class colors


//private fields
class Color1 {
    // Declare: every Color instance has a private field called #values.
    #values;
    constructor(r, g, b) {
      this.#values = [r, g, b];
    }
    getRed() {
      return this.#values[0];
    }
    setRed(value) {
      this.#values[0] = value;
    }
  }
  const red2 = new Color1(255, 0, 0);
  console.log('from private field: ',red2.getRed()); // 255
  //console.log(red.#values); //this will not work because #values is private 
  


  //accessor fileds
  class Color3 {
    constructor(r, g, b) {
      this.values = [r, g, b];
    }
    get red() {
      return this.values[0];
    }
    set red(value) {
      this.values[0] = value;
    }
  }
  const red5 = new Color3(255, 0, 0);
  red.red5 = 0;
  console.log('accessor field: ',red.red5); 


  //public fields
  class MyClass1 {
    luckyNumber = Math.random();
  }
  console.log('public fields: ');
  console.log(new MyClass1().luckyNumber); 
  console.log(new MyClass1().luckyNumber);
  

//static properties
class Color5 {
    static isValid(r, g, b) {
      return r >= 0 && r <= 255 && g >= 0 && g <= 255 && b >= 0 && b <= 255;
    }
  }
  console.log('static fields: ');
  console.log(Color5.isValid(255, 0, 0)); 
  console.log(Color5.isValid(1000, 0, 0));

  
//Extends and inheritance  
class Color7 {
    #values;
    constructor(r, g, b, a = 1) {
      this.#values = [r, g, b, a];
    }
    get alpha() {
      return this.#values[3];
    }
    set alpha(value) {
      if (value < 0 || value > 1) {
        throw new RangeError("Alpha value must be between 0 and 1");
      }
      this.#values[3] = value;
    }
  }
  
class ColorWithAlpha extends Color7 {
    #alpha;
    constructor(r, g, b, a) {
      super(r, g, b);
      this.#alpha = a;
    }
    get alpha() {
      return this.#alpha;
    }
    set alpha(value) {
      if (value < 0 || value > 1) {
        throw new RangeError("Alpha value must be between 0 and 1");
      }
      this.#alpha = value;
    }
  }
  const color9 = new ColorWithAlpha(255, 0, 0, 0.5);
  console.log(color9.alpha); 