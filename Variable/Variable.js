
console.log('This is the code Examples for variables');
//For var
var a=5;
console.log('The value of a is ' + a); 
var b=6;
console.log('The value of b is ' + b); 
var c=2;
console.log('The value of c is ' + b); 
//For let
let x=0;
console.log('The value of x is ' + x);
let z=9;
console.log('The value of z is ' + z); 
//For const
const val=15;
console.log('The value of val is ' + val);
//the value of this const variable will not be changed the below code will give error
/*
val=17
 */

//variable scope
//The scope of t is not limited to the immediate if statement block.
if (true) {
    var t = 5;
  }
  console.log('Value of t is ' + t); 
//The concept hoisting.
// you can refer to  variable declared later, without getting an exception.
console.log(h === undefined); // true
var h = 3;
console.log('The hoisting variable is ' + h);
//
// will return a value of undefined
var myvar = 'my value';

(function() {
  console.log(myvar); // undefined
  var myvar = 'k';
  console.log('hoisting variable is ' + myvar);
})();


//DataType conversion
//Number and '+' the operator
var ans = ' Hi';
ans = 22 + ans;console.log('Number and '+' the operator: ' + ans);

//Javascript can convert numerical value to string
var str = '222';
str = 222+str;
console.log('numeric value is ' + str);
//Strings to number
console.log(parseInt('101',2));


//Literals
//Array literals
// the type of  the object , the objects are literals
let coffees = ['French Roast', 'Colombian', 'Kona'];
console.log('Array literals val ' + coffees);


