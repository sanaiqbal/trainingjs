//Loops 
//For loops
var count =0;
for(let i=0;i<5;i++)
{
  count++;
}
console.log('value of variable after for loop ' + count);

//do while
var a =100;
var b = 7;
var count1=0;
do{
   count1= a+b+count1+2;
   a--;
}while(a<b);
console.log('value of variable after do while loop '+count1);

//while 
var x=0;
var y=10;
var count3=0;
marktheloop:
while(y>x)
{
  count3++;
  x++;
  y--;
}
console.log('value of variable after while loop '+count3);

//Break 
var br=7;
for(let i=0;i<15 ;i++)
{
   if(i>br)
   { 
    br=i;
    break;
   }
   else
   {
      console.log('loop is not break ');
   }
}
console.log('loop is break at '+br);
//Continue
var br=7;
checkiandbr:
for(let i=0;i<15 ;i++)
{
   if(i>br)
   { 
    br=i;
    continue;
   }
   else
   {
      console.log('loop is not continue at '+i);
   }
}
console.log('loop is continue at '+br);
// for in loop
const person = {fname:"John", lname:" Doe ", age:25};

let text = "";
for (let x in person) {
  text += person[x];
}
console.log('for in loop output '+text);

//for of loop
const arr = [3, 5, 7];
arr.foo = 'hello';

for (const i in arr) {
  console.log(i); // logs "0", "1", "2", "foo"
}

for (const i of arr) {
  console.log(i); // logs 3, 5, 7
}